insert into public.user_details(id, first_name, last_name, birth_date, sex)
values(10001, 'Ania', 'Ploszaj', '1998-09-11', 1),
(10002, 'Marcin', 'Wilczynski', '1998-05-09', 0),
(10003, 'Marek', 'Ochamnn', '1998-10-20', 0),
(10004, 'Klaudia', 'Dyrka', '1998-06-10', 1);

insert into public.posts(id, description, user_id)
values(20001, 'test post 1', 10001);