package com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository;

import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
}
