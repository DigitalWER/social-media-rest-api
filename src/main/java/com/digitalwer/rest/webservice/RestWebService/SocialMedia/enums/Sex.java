package com.digitalwer.rest.webservice.RestWebService.SocialMedia.enums;

public enum Sex {
    MALE, FEMALE, OTHER
}
