package com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository;

import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.Interest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InterestRepository extends JpaRepository<Interest, Long> {
    Interest findByName(String name);
}
