package com.digitalwer.rest.webservice.RestWebService.SocialMedia.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class AppUserNotFoundException extends RuntimeException{
    private static final String USER_NOT_FOUND_MESSAGE = "User not found";

    public AppUserNotFoundException(){
        super(USER_NOT_FOUND_MESSAGE);
    }

    public AppUserNotFoundException(Long id){
        super(USER_NOT_FOUND_MESSAGE + " with id " + id);
    }
}
