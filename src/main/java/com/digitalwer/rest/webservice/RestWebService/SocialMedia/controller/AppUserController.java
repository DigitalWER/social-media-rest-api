package com.digitalwer.rest.webservice.RestWebService.SocialMedia.controller;

import com.digitalwer.rest.webservice.RestWebService.SocialMedia.configuration.AppUserService;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.exceptions.AppUserNotFoundException;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.AppUser;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.Post;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository.AppUserRepository;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("/users")
public class AppUserController {

    private AppUserRepository repository;
    private AppUserService service;

    @PostMapping("/create")
    public ResponseEntity<AppUser> createUser(@Valid @RequestBody AppUser user) {
        service.saveUser(user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .replacePath("/users/{id}")
                .buildAndExpand(user.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/all")
    public List<AppUser> getUsers() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public EntityModel<AppUser> getUserById(@PathVariable Long id) {
        Optional<AppUser> user = repository.findById(id);
        if (user.isEmpty())
            throw new AppUserNotFoundException(id);
        EntityModel<AppUser> entityModel = EntityModel.of(user.get());
        WebMvcLinkBuilder link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getUsers());
        entityModel.add(link.withRel("all-users"));
        return entityModel;
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUserById(@PathVariable Long id) {
        repository.deleteById(id);
    }

    @GetMapping("/{id}/get-posts")
    public List<Post> getAllPostsForSpecificUser(@PathVariable Long id){
        Optional<AppUser> user = repository.findById(id);

        if (user.isEmpty())
            throw new AppUserNotFoundException();

        return user.get().getPosts();
    }
}

