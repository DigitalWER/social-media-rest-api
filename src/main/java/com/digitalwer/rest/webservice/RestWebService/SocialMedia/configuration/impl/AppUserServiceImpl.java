package com.digitalwer.rest.webservice.RestWebService.SocialMedia.configuration.impl;

import com.digitalwer.rest.webservice.RestWebService.SocialMedia.configuration.AppUserService;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.exceptions.AppUserNotFoundException;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.AppUser;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.Interest;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository.AppUserRepository;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository.InterestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class AppUserServiceImpl implements AppUserService {
    @Autowired
    private AppUserRepository userRepository;
    @Autowired
    private InterestRepository interestRepository;

    public void saveUser(AppUser user) {
        Set<Interest> uniqueInterests = new HashSet<>();
        Set<Interest> inputInterests = user.getInterests();
        for (Interest inputInterest : inputInterests) {
            Interest existingInterest = interestRepository.findByName(inputInterest.getName());
            if (existingInterest == null) {
                interestRepository.save(inputInterest);
                uniqueInterests.add(inputInterest);
            } else {
                uniqueInterests.add(existingInterest);
            }
        }
        user.setInterests(uniqueInterests);
        userRepository.save(user);
    }
}