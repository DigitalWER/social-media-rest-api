package com.digitalwer.rest.webservice.RestWebService.SocialMedia.configuration;

import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.AppUser;

import java.util.List;
import java.util.Optional;

public interface AppUserService {
    void saveUser(AppUser user);
}
