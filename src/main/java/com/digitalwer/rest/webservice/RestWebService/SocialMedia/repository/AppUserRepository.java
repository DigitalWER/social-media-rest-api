package com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository;

import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
//    AppUser findByEmail(String email);
}
