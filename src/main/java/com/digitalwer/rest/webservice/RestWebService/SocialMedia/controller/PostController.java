package com.digitalwer.rest.webservice.RestWebService.SocialMedia.controller;

import com.digitalwer.rest.webservice.RestWebService.SocialMedia.exceptions.AppUserNotFoundException;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.AppUser;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.model.Post;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository.AppUserRepository;
import com.digitalwer.rest.webservice.RestWebService.SocialMedia.repository.PostRepository;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("/posts")
public class PostController {
    private PostRepository repository;
    private AppUserRepository userRepository;

    @PostMapping("/create/user/{id}")
    public ResponseEntity<Post> createPostForUser(@Valid @RequestBody Post post, @PathVariable Long id) {
        Optional<AppUser> user = userRepository.findById(id);
        if (user.isEmpty()){
            throw new AppUserNotFoundException();
        }

        post.setUser(user.get());
        repository.save(post);
        URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .replacePath("/posts/{id}")
                .buildAndExpand(post.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/all")
    public List<Post> getPosts() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public EntityModel<Post> getPostById(@PathVariable Long id) {
        Optional<Post> post = repository.findById(id);
        if (post == null)
            throw new AppUserNotFoundException(id);
        EntityModel<Post> entityModel = EntityModel.of(post.get());
        WebMvcLinkBuilder link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getPosts());
        entityModel.add(link.withRel("all-posts"));
        return entityModel;
    }

    @DeleteMapping("delete/{id}")
    public void deletePostById(@PathVariable Long id) {
        repository.deleteById(id);
    }
}

