package com.digitalwer.rest.webservice.RestWebService.SocialMedia.model;

import jakarta.persistence.*;
import lombok.*;

@ToString
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Interest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Interest(String name){
        this.name = name;
    }
}
