package com.digitalwer.rest.webservice.RestWebService.SocialMedia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity(name = "posts")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Post {

    @GeneratedValue
    @Id
    private Long id;

    @Size(min = 10)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private AppUser user;
}
